package com.clearos.app.commons.interfaces

interface HashListener {
    fun receivedHash(hash: String, type: Int)
}
