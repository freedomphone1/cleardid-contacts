package com.clearos.clearnames

import androidx.core.content.ContextCompat
import com.clearos.app.commons.extensions.checkUseEnglish
import com.clearos.clearnames.helpers.Config

fun Config.accentColor(): Int {
    return ContextCompat.getColor(context, com.clearos.clearnames.R.color.color_accent)
}
class App : BaseApplication() {
    override fun onCreate() {
        super.onCreate()
        checkUseEnglish()
    }
}
