package com.clearos.clearnames.helpers;

import io.ipfs.multihash.Multihash;

public class EncryptionBlock {
    public byte[] cipher;
    public Multihash cid;

    public EncryptionBlock(byte[] _cipher, Multihash _cid) {
        cipher = _cipher;
        cid = _cid;
    }
}

