package com.clearos.clearnames.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.clearos.clearnames.R
import com.clearos.clearnames.activities.SimpleActivity
import com.clearos.app.commons.extensions.getAdjustedPrimaryColor
import com.clearos.clearnames.extensions.config
import kotlinx.android.synthetic.main.item_menu_option.view.*

class MenuAdapter(val activity: SimpleActivity, private val options: List<String>, private val callback: (String) -> Unit) :
    RecyclerView.Adapter<MenuAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = activity.layoutInflater.inflate(R.layout.item_menu_option, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: MenuAdapter.ViewHolder, position: Int) {
        holder.bindView(options[position])
    }

    override fun getItemCount(): Int = options.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindView(option: String): View {
            itemView.apply {
                item_menu_option_text.setColors(activity.config.textColor, activity.getAdjustedPrimaryColor(), activity.config.backgroundColor)
                item_menu_option_text.text = option
                item_menu_option_holder.setOnClickListener { viewClicked(option) }
            }

            return itemView
        }

        private fun viewClicked(option: String) {
            callback(option)
        }
    }
}
