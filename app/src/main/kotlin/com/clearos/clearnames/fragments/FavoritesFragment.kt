package com.clearos.clearnames.fragments

import android.content.Context
import android.util.AttributeSet
import com.clearos.app.commons.helpers.TAB_FAVORITES
import com.clearos.clearnames.activities.MainActivity
import com.clearos.clearnames.activities.SimpleActivity
import com.clearos.clearnames.dialogs.SelectContactsDialog
import com.clearos.clearnames.helpers.ContactsHelper

class FavoritesFragment(context: Context, attributeSet: AttributeSet) : MyViewPagerFragment(context, attributeSet) {
    override fun fabClicked() {
        finishActMode()
        showAddFavoritesDialog()
    }

    override fun placeholderClicked() {
        showAddFavoritesDialog()
    }

    private fun showAddFavoritesDialog() {
        SelectContactsDialog(activity!!, allContacts, true, false) { addedContacts, removedContacts ->
            ContactsHelper(activity as SimpleActivity).apply {
                addFavorites(addedContacts)
                removeFavorites(removedContacts)
            }

            (activity as? MainActivity)?.refreshContacts(TAB_FAVORITES)
        }
    }
}
