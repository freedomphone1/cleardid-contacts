package com.clearos.clearnames.fragments

import android.content.Context
import android.util.AttributeSet
import com.clearos.app.commons.helpers.TAB_GROUPS
import com.clearos.clearnames.activities.MainActivity
import com.clearos.clearnames.activities.SimpleActivity
import com.clearos.clearnames.dialogs.CreateNewGroupDialog

class GroupsFragment(context: Context, attributeSet: AttributeSet) : MyViewPagerFragment(context, attributeSet) {
    override fun fabClicked() {
        finishActMode()
        showNewGroupsDialog()
    }

    override fun placeholderClicked() {
        showNewGroupsDialog()
    }

    private fun showNewGroupsDialog() {
        CreateNewGroupDialog(activity as SimpleActivity) {
            (activity as? MainActivity)?.refreshContacts(TAB_GROUPS)
        }
    }
}
