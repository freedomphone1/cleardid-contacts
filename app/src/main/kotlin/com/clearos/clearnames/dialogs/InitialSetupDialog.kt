package com.clearos.clearnames.dialogs

import androidx.appcompat.app.AlertDialog
import com.clearos.clearnames.R
import com.clearos.app.commons.activities.BaseSimpleActivity
import com.clearos.app.commons.extensions.setupDialogStuff

class InitialSetupDialog(val activity: BaseSimpleActivity, val callback: (Boolean) -> Unit) {
    private var dialog: AlertDialog? = null
    private var view = activity.layoutInflater.inflate(R.layout.dialog_initial_setup,null)

    init {
        dialog = AlertDialog.Builder(activity)
            .setPositiveButton(R.string.yes) { dialogInterface, i ->
                callback(true)
                dialog?.dismiss()
            }
            .setNegativeButton(R.string.no) { dialogInterface, i ->
                callback(false)
                dialog?.dismiss()
            }
            .create().apply {
                activity.setupDialogStuff(view, this)
                setOnCancelListener {
                    callback(false)
                    dismiss()
                }
            }
    }
}
