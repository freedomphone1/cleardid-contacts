package com.clearos.clearnames.dialogs

import androidx.appcompat.app.AlertDialog
import com.clearos.clearnames.R
import com.clearos.clearnames.activities.SimpleActivity
import com.clearos.clearnames.adapters.FilterContactSourcesAdapter
import com.clearos.clearnames.adapters.MenuAdapter
import com.clearos.app.commons.extensions.setupDialogStuff
import com.clearos.clearnames.extensions.getVisibleContactSources
import com.clearos.clearnames.helpers.ContactsHelper
import kotlinx.android.synthetic.main.dialog_filter_contact_sources.view.*
import java.util.ArrayList

class MenuDialog(val activity: SimpleActivity, val options: ArrayList<String>, val callback: (String) -> Unit) {
    private var dialog: AlertDialog? = null
    private val view = activity.layoutInflater.inflate(R.layout.dialog_filter_contact_sources, null)

    private val checkDismissCallback = { option: String ->
        if (option != activity.getString(R.string.filter)) {
            dialog?.dismiss()
        }
        callback(option)
    }

    init {
        view.filter_contact_sources_list.adapter = MenuAdapter(activity, options, checkDismissCallback)

        dialog = AlertDialog.Builder(activity)
            .setNegativeButton(R.string.cancel, null)
            .create().apply {
                activity.setupDialogStuff(view, this)
            }
    }
}
