package com.clearos.clearnames.dialogs

import android.graphics.Bitmap
import androidx.appcompat.app.AlertDialog
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.clearos.app.commons.activities.BaseSimpleActivity
import com.clearos.app.commons.extensions.copyToClipboard
import com.clearos.app.commons.extensions.setupDialogStuff
import com.clearos.clearnames.R
import kotlinx.android.synthetic.main.dialog_view_qr_code.view.*

class ViewQrCodeDialog(val activity: BaseSimpleActivity, val qrCodeValue: String) {
    private var view = activity.layoutInflater.inflate(R.layout.dialog_view_qr_code, null)

    companion object {
        private const val WIDTH = 250
        private const val PLUM = 0xFF694E70
        private const val WHITE = 0xFFFFFFFF
    }

    init {
        try {
            val bitmap: Bitmap? = encodeAsBitmap(qrCodeValue)
            view.qr_code.setImageBitmap(bitmap)
        } catch (e: WriterException) {
            e.printStackTrace()
        }

        AlertDialog.Builder(activity)
            .setPositiveButton(R.string.copy) { dialog, which -> copyToClipboard() }
            .setNegativeButton(R.string.cancel, null)
            .create().apply {
                activity.setupDialogStuff(view, this) {
                    view.qr_code.apply {
                        setOnClickListener {
                            dismiss()
                        }
                    }
                }
            }
    }

    @Throws(WriterException::class)
    private fun encodeAsBitmap(str: String?): Bitmap? {
        val result: BitMatrix
        result = try {
            MultiFormatWriter().encode(
                str,
                BarcodeFormat.QR_CODE, WIDTH, WIDTH, null
            )
        } catch (iae: IllegalArgumentException) {
            // Unsupported format
            return null
        }
        val w = result.width
        val h = result.height
        val pixels = IntArray(w * h)
        for (y in 0 until h) {
            val offset = y * w
            for (x in 0 until w) {
                pixels[offset + x] = (if (result[x, y]) PLUM else WHITE).toInt()
            }
        }
        val bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        bitmap.setPixels(pixels, 0, WIDTH, 0, 0, w, h)
        return bitmap
    }

    private fun copyToClipboard() {
        activity.copyToClipboard(qrCodeValue)
    }
}
