package com.clearos.clearnames.dialogs

import androidx.appcompat.app.AlertDialog
import android.content.Intent
import com.clearos.clearnames.R
import com.clearos.clearnames.activities.SettingsActivity
import com.clearos.app.commons.activities.BaseSimpleActivity
import com.clearos.app.commons.extensions.setupDialogStuff
import kotlinx.android.synthetic.main.dialog_auto_backup_reminder.view.*

class AutoBackupReminderDialog(val activity: BaseSimpleActivity) {
    private var dialog: AlertDialog? = null
    private var view = activity.layoutInflater.inflate(R.layout.dialog_auto_backup_reminder,null)

    init {
        dialog = AlertDialog.Builder(activity)
            .setPositiveButton(R.string.yes) { dialogInterface, i -> goToSettings() }
            .setNegativeButton(R.string.no) { dialogInterface, i -> dialog?.dismiss() }
            .create().apply {
                activity.setupDialogStuff(view, this)
            }
    }

    private fun goToSettings() {
        activity.startActivity(
            Intent(
                activity.applicationContext,
                SettingsActivity::class.java
            )
        )
        dialog?.dismiss()
    }
}
