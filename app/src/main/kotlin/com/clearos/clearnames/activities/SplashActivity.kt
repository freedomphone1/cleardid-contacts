package com.clearos.clearnames.activities

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import com.clearos.app.commons.activities.BaseSplashActivity
import com.clearos.clearnames.BaseApplication
import com.clearos.clearnames.R
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar

class SplashActivity : BaseSplashActivity() {
    private var progressBar: ProgressBar? = null
    private var statusLabel: TextView? = null
    private val TAG = "SplashActivity"
    override fun initActivity() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        progressBar = findViewById(R.id.progressBar)
        statusLabel = findViewById(R.id.txtStatusLabel)
        setUpdateUiElements(progressBar, statusLabel)

        setRequiredPermissions(
            arrayOf(
                getString(R.string.DerivedKeyPermission),
                getString(R.string.AppUpdatePermission),
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.WRITE_CONTACTS
            )
        )
        setUpdateSegue { segue() }
        setAppRequiresInternet(false)
    }

    private fun segue() {
        Log.d(TAG, "Setting up user keys for base application.")
        BaseApplication.instance!!.setupKeys(this.packageName, { didKeys ->
            if (didKeys != null) {
                Handler(mainLooper).post {
                    Log.d(TAG, "Derived keys were setup with a DID " + didKeys.did)
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
            } else {
                popupSnackbarForError()
            }

            null
        }, null, false)
    }

    private fun popupSnackbarForError() {
        val snackbar = Snackbar.make(
            findViewById(android.R.id.content),
            getString(R.string.noDerivedKeysError),
            4000 // 4 seconds
        )
        snackbar.setTextColor(getColor(R.color.white))
        snackbar.setAction("OK") { startClearLife() }
        snackbar.setActionTextColor(
            getColor(R.color.white)
        )
        snackbar.addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
            override fun onShown(transientBottomBar: Snackbar?) {
                super.onShown(transientBottomBar)
            }

            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                super.onDismissed(transientBottomBar, event)
                startClearLife()
            }
        })
        snackbar.show()
    }

    private fun startClearLife() {
        val launchIntent =
            applicationContext.packageManager.getLaunchIntentForPackage("com.clearos.clearlife")
        if (launchIntent != null) {
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
            startActivity(launchIntent)
            finishAffinity()
        }
    }
}
