package com.clearos.clearnames.models

import android.content.Context
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.goterl.lazysodium.utils.KeyPair
import com.clearos.clearnames.BaseApplication
import com.clearos.app.commons.models.PhoneNumber
import com.clearos.app.commons.overloads.times
import com.clearos.clearnames.extensions.contactsDB
import io.ipfs.multibase.Base58

@Entity(tableName = "contacts", indices = [(Index(value = ["id"], unique = true))])
data class LocalContact(
    @PrimaryKey(autoGenerate = true) var id: Int?,
    @ColumnInfo(name = "did") var did: String,
    @ColumnInfo(name = "wallet") var wallet: String,
    @ColumnInfo(name = "key_context") var keyContext: Int,
    @ColumnInfo(name = "prefix") var prefix: String,
    @ColumnInfo(name = "first_name") var firstName: String,
    @ColumnInfo(name = "middle_name") var middleName: String,
    @ColumnInfo(name = "surname") var surname: String,
    @ColumnInfo(name = "suffix") var suffix: String,
    @ColumnInfo(name = "nickname") var nickname: String,
    @ColumnInfo(name = "photo", typeAffinity = ColumnInfo.BLOB) var photo: ByteArray?,
    @ColumnInfo(name = "photo_uri") var photoUri: String,
    @ColumnInfo(name = "phone_numbers") var phoneNumbers: ArrayList<PhoneNumber>,
    @ColumnInfo(name = "emails") var emails: ArrayList<Email>,
    @ColumnInfo(name = "events") var events: ArrayList<Event>,
    @ColumnInfo(name = "starred") var starred: Int,
    @ColumnInfo(name = "addresses") var addresses: ArrayList<Address>,
    @ColumnInfo(name = "notes") var notes: String,
    @ColumnInfo(name = "groups") var groups: ArrayList<Long>,
    @ColumnInfo(name = "company") var company: String,
    @ColumnInfo(name = "job_position") var jobPosition: String,
    @ColumnInfo(name = "websites") var websites: ArrayList<String>,
    @ColumnInfo(name = "ims") var IMs: ArrayList<IM>,
    @ColumnInfo(name = "ringtone") var ringtone: String?) {

    /**
     * Creates the key context bytes needed to derive the specific key for this contact.
     * Must be at least 8 bytes long.
     */
    private fun getLocalKeyContext(context: Context): String {
        val REQUIRED_SIZE = 8

        if (keyContext == -1) {
            val maxKeyContext = context.contactsDB.getMaximumKeyContext(firstName, middleName, surname)
            keyContext = maxKeyContext + 1
            context.contactsDB.insertOrUpdate(this)
        }

        var localKeyContext = String.format("%s:%s:%s:%d", firstName, middleName, surname, keyContext)
        val keyContextSize = localKeyContext.toByteArray().size

        if (keyContextSize < REQUIRED_SIZE) {
            val spacesNeeded = REQUIRED_SIZE - keyContextSize
            firstName += (" " * spacesNeeded)
            localKeyContext = String.format("%s:%s:%s:%d", firstName, middleName, surname, keyContext)
        }

        return localKeyContext
    }

    /**
     * Returns the derived key pair for this relationship/contact.
     */
    fun getDerivedKey(context: Context, keyId: Long = 0): KeyPair {
        return BaseApplication.instance!!.keyClient.deriveKeyPair(getLocalKeyContext(context), keyId)
    }

    /**
     * Returns the public key.
     */
    fun getPublicKey(context: Context): String {
        val key = getDerivedKey(context)
        return Base58.encode(key.publicKey.asBytes)
    }

    override fun equals(other: Any?) = id == (other as? LocalContact?)?.id

    override fun hashCode() = id ?: 0
}
