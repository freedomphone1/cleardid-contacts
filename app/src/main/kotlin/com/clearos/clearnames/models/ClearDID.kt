package com.clearos.clearnames.models

import android.graphics.Bitmap
import android.graphics.Color
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.common.BitMatrix


object ClearDID {

    val CONTENT_ITEM_TYPE = "vnd.android.cursor.item/did"
    val DATA = "data1"

    fun didValueToQR(didValue: String, size: Int = 200): Bitmap? {
        val bitMatrix: BitMatrix = try {
            MultiFormatWriter().encode(
                    didValue,
                    BarcodeFormat.QR_CODE,
                    size, size, null
            )
        } catch (e: IllegalArgumentException) {
            return null
        }
        val bitMatrixWidth = bitMatrix.width
        val bitMatrixHeight = bitMatrix.height
        val bitmap: Bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_8888)

        for (y in 0 until bitMatrixHeight) {
            for (x in 0 until bitMatrixWidth) {
                bitmap.setPixel(y, x, if (bitMatrix[y, x]) Color.BLACK else Color.WHITE)
            }
        }
        return bitmap
    }
}
