package com.clearos.clearnames.models

import android.graphics.Bitmap
import android.graphics.Color
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.common.BitMatrix

object Wallet {

    val CONTENT_ITEM_TYPE = "vnd.android.cursor.item/wallet"
    val DATA = "data2"

    fun walletValueToQR(walletValue: String, size: Int = 200): Bitmap? {
        val bitMatrix: BitMatrix = try {
            MultiFormatWriter().encode(
                walletValue,
                BarcodeFormat.QR_CODE,
                size, size, null
            )
        } catch (e: IllegalArgumentException) {
            return null
        }
        val bitMatrixWidth = bitMatrix.width
        val bitMatrixHeight = bitMatrix.height
        val bitmap: Bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_8888)

        for (y in 0 until bitMatrixHeight) {
            for (x in 0 until bitMatrixWidth) {
                bitmap.setPixel(y, x, if (bitMatrix[y, x]) Color.BLACK else Color.WHITE)
            }
        }
        return bitmap
    }
}
