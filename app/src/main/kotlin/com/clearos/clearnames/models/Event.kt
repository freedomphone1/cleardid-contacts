package com.clearos.clearnames.models

data class Event(var value: String, var type: Int)
