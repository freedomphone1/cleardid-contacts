package com.clearos.clearnames

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.clearos.dstorage.DecentralizedStorageApplication
import java.io.File
import java.io.IOException

open class BaseApplication : DecentralizedStorageApplication() {
    override fun onCreate() {
        super.onCreate()
        if (instance == null) {
            instance = this
        }
    }

    override fun onIOError(e: IOException?) {
        super.onIOError(e)
        Toast.makeText(applicationContext, "Storage Action Failed", Toast.LENGTH_LONG).show()
    }

    // Called when backup file has completed loading into local storage
    override fun onFileLoaded(file: File) {
        super.onFileLoaded(file)
        Log.d("ClearDID.BaseApp", "File of ${file.name} has been loaded locally")
        backupFile.postValue(file)
    }

    companion object {
        @get:Synchronized
        var instance: BaseApplication? = null
            private set

        val backupFile = MutableLiveData<File>()
    }

}
