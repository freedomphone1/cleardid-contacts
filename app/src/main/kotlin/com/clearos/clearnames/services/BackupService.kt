package com.clearos.clearnames.services

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.net.Uri
import android.os.IBinder
import android.util.Log
import android.util.Pair
import androidx.core.app.NotificationCompat
import com.clearos.clearnames.BaseApplication.Companion.instance
import com.clearos.clearnames.R
import com.clearos.clearnames.activities.MainActivity
import com.clearos.app.commons.helpers.ensureBackgroundThread
import com.clearos.clearnames.helpers.ContactsHelper
import com.clearos.clearnames.helpers.VcfExporter
import java.io.File
import java.io.FileNotFoundException
import java.io.OutputStream
import java.util.*

class BackupService : Service() {

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)
        val notification = NotificationCompat.Builder(this, MainActivity.BACKUP_CHANNEL_ID)
            .setContentTitle("ClearDID Backup")
            .setContentText("Backing up contacts to DID")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(pendingIntent)
            .setProgress(100, 0, true)
            .setSound(null)
            .build()
        startForeground(1, notification)

        //Create the file to store contacts
        val realPath = applicationContext.filesDir!!.absolutePath
        val file = File(realPath, backupFileName)

        // Do heavy work in background thread (Recomended)
        ensureBackgroundThread {
            val ignoredSources = HashSet<String>(0)
            try {
                val outputStream = contentResolver.openOutputStream(Uri.fromFile(file))
                Log.d(TAG, "Exporting Contacts")
                exportContactsTo(ignoredSources, outputStream)
                Log.d(TAG, "Contacts exported successfully")
                Log.d(TAG, "Uploading backup from directory: $realPath")

                // Setup a Client from the decentralized Storage Application
                instance!!.setupStorageClient(
                    instance!!.keyClient.appContext.packageName
                ) {
                    // Setup file sharing
                    instance!!.configureDataStores(
                        "com.clearos.clearnames.provider",
                        R.xml.provider_paths
                    )

                    // Save file to storage
                    Log.d(TAG, "Uploading file: $backupFileName")
                    instance!!.getDataStore(getString(R.string.app_name))
                        .save(Pair(backupFileName, file))

                    // Kill service
                    Log.d(TAG, "Killing Backup service")
                    stopSelf()
                    null
                }
            } catch (e: FileNotFoundException) {
                Log.e(TAG, "Problem exporting contacts")
                e.printStackTrace()
            }
        }

        return START_NOT_STICKY
    }

    private fun exportContactsTo(
        ignoredContactSources: HashSet<String>,
        outputStream: OutputStream?
    ) {
        ContactsHelper(this).getContacts(
            getAll = true,
            gettingDuplicates = false,
            ignoredContactSources = ignoredContactSources
        ) { contacts ->
            if (contacts.isNotEmpty()) {
                VcfExporter().exportContacts(this, outputStream, contacts, true) { null }
            }
            null
        }
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    companion object {
        private const val TAG = "BackupService"
        private const val backupFileName = "ClearDID.vcf"
    }
}
