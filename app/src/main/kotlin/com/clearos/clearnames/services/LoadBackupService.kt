package com.clearos.clearnames.services

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import com.clearos.clearnames.BaseApplication.Companion.instance
import com.clearos.clearnames.R
import com.clearos.clearnames.activities.MainActivity
import com.clearos.app.commons.helpers.ensureBackgroundThread

open class LoadBackupService : Service() {
    private val TAG = "LoadBackupService"
    private val backupFileName = "ClearDID.vcf"

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)


        val notification = NotificationCompat.Builder(this, MainActivity.RESTORE_CHANNEL_ID)
            .setContentTitle("ClearDID Restore")
            .setContentText("Restoring contacts from DID")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(pendingIntent)
            .setProgress(100, 0, true)
            .setSound(null)
            .build()

        startForeground(1, notification)

        // Do work in the background (recomended)
        ensureBackgroundThread {
            // Setup a StorageClient to connect to service
            instance!!.setupStorageClient(
                instance!!.keyClient.appContext.packageName
            ) {
                // Load the file from decentralized Storage
                Log.d(TAG, "Loading backup file of: $backupFileName")

                // Configure file paths in case it has not been done
                instance!!.configureDataStores(
                    "com.clearos.clearnames.provider",
                    R.xml.provider_paths
                )

                // Get an application store and load/download file
                instance!!.getDataStore(getString(R.string.app_name))
                    .load(backupFileName)

                // kill the service
                Log.d(TAG, "Killing Restore Service")
                stopSelf()

                //The callback requires a return of null
                null
            }
        }

        return START_NOT_STICKY
    }

    // Binder for Main Activity to set handler
    override fun onBind(p0: Intent?): IBinder? {
        return null
    }
}
