package com.clearos.clearnames.interfaces

import com.clearos.clearnames.models.Contact

interface RemoveFromGroupListener {
    fun removeFromGroup(contacts: ArrayList<Contact>)
}
