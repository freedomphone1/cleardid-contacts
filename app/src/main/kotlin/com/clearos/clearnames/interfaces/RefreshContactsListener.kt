package com.clearos.clearnames.interfaces

import com.clearos.clearnames.models.Contact

interface RefreshContactsListener {
    fun refreshContacts(refreshTabsMask: Int)

    fun contactClicked(contact: Contact)
}
