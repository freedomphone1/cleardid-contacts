package com.clearos.clearnames.interfaces

import androidx.room.*
import androidx.sqlite.db.SupportSQLiteQuery
import com.clearos.clearnames.models.LocalContact

@Dao
interface ContactsDao {
    @RawQuery
    fun genericQuery(supportSQLiteQuery: SupportSQLiteQuery): Int

    @Query("SELECT * FROM contacts")
    fun getContacts(): List<LocalContact>

    // Whenever a new contact gets added, we query the database to see if any contacts exist that
    // have exactly the same name. If there is one, we find the maximum key context assigned to that
    // name already and then we'll use that result `+1` as the key context for the new contact.
    @Query("SELECT MAX(key_context) FROM CONTACTS WHERE first_name == :firstName AND middle_name == :middleName AND surname == :surname")
    fun getMaximumKeyContext(firstName: String, middleName: String, surname: String): Int

    @Query("SELECT * FROM contacts WHERE starred = 1")
    fun getFavoriteContacts(): List<LocalContact>

    @Query("SELECT * FROM contacts WHERE id = :id")
    fun getContactWithId(id: Int): LocalContact?

    @Query("SELECT * FROM contacts WHERE phone_numbers LIKE :number")
    fun getContactWithNumber(number: String): LocalContact?

    @Query("UPDATE contacts SET starred = :isStarred WHERE id = :id")
    fun updateStarred(isStarred: Int, id: Int)

    @Query("UPDATE contacts SET ringtone = :ringtone WHERE id = :id")
    fun updateRingtone(ringtone: String, id: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdate(contact: LocalContact): Long

    @Query("DELETE FROM contacts WHERE id = :id")
    fun deleteContactId(id: Int)

    @Query("DELETE FROM contacts WHERE id IN (:ids)")
    fun deleteContactIds(ids: List<Long>)
}
