package com.clearos.clearnames.interfaces;

import com.clearos.dstorage.BlockHashFile;
import com.clearos.dstorage.ProgressResult;

import java.util.function.Function;

public interface IProgressHandler {
    /**
     * Event that fires when a new block upload/download starts.
     */
    void onBlockStarted(ProgressResult result);

    /**
     * Event that fires whenever the total progress of the upload/download changes.
     */
    void onTotalProgress(Float percentDone);

    /**
     * Event that fires whenever a block is finished (either success or failure).
     */
    void onBlockProgress(ProgressResult result);

    /**
     * Adds an additional callback function to fire whenever block progress fires.
     */
    void addProgressCallback(Function<ProgressResult, Void> callback);

    /**
     * Event called when the whole file is completely downloaded/uploaded.
     */
    void onProcessCompleted(BlockHashFile completeFile);
}

