package com.clearos.clearnames.interfaces;

import android.os.CancellationSignal;

import com.clearos.dstorage.BlockHashFile;
import com.clearos.dstorage.ProgressResult;
import com.goterl.lazysodium.exceptions.SodiumException;
import com.goterl.lazysodium.utils.KeyPair;

import java.io.IOException;
import java.util.function.Function;

import io.ipfs.multihash.Multihash;

public interface BlockWriter {
    void onBegin(IProgressHandler progressHandler, CancellationSignal signal, int totalExpectedBlocks);
    void checkUpload(Uploader upload);
    void onNext(byte[] cipher, Multihash cid, int blockIndex);
    void onIOError(IOException error);
    void onEncryptionError(SodiumException error);
    void onGenericError(Throwable error);
    void onIndexUpdated(long uploadedBytes);
    void onCompleted(BlockHashFile file, Function<BlockHashFile, Void> callback);
    KeyPair getBaseKeys();
    void setConcurrencyCallback(Function<ProgressResult, Void> callback);
}

